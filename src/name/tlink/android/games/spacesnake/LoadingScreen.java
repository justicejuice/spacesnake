package name.tlink.android.games.spacesnake;

import name.tlink.android.game.framework.Graphics.PixmapFormat;
import name.tlink.android.game.framework.Game;
import name.tlink.android.game.framework.Screen;
import name.tlink.android.game.framework.Audio;
import name.tlink.android.game.framework.Graphics;

/**
 * This class would show a loading screen, but the assets are loading so fast,
 * it isn't necessary. So it simply loads all Assets from sd card and creates the
 * instances. When finished loading all Assets the {@link MainMenuScreen} will be shown.
 * @author Timon Link <a href="mailto:timon.link@gmail.com">timon.link@gmail.com</a>
 * @date 25.08.2014
 * @version 1.0
 */
public class LoadingScreen extends Screen {

	/**
	 * Creates a new instance of loading screen.
	 * @param game
	 * 	The current {@link Game} instance.
	 */
	public LoadingScreen(Game game) {
		super(game);
	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void present(float arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	/**
	 * Loads all {@link Assets} and wrapps them into the 
	 * corresponding objects. When all {@link Assets} are loaded
	 * the {@link MainMenuScreen} is set to the screen.
	 */
	@Override
	public void update(float deltaTime) {
		Graphics g = game.getGraphics();
		Audio a = game.getAudio();
		
		Assets.img_arrow_left = g.newPixmap("img/arrow_left.png", PixmapFormat.ARGB4444);
		Assets.img_arrow_right = g.newPixmap("img/arrow_right.png", PixmapFormat.ARGB4444);
		Assets.img_background = g.newPixmap("img/background.png", PixmapFormat.RGB565);
		Assets.img_frame_left = g.newPixmap("img/frame_left.png", PixmapFormat.ARGB4444);
		Assets.img_frame_right = g.newPixmap("img/frame_right.png", PixmapFormat.ARGB4444);
		Assets.img_game_menu = g.newPixmap("img/game_menu.png", PixmapFormat.ARGB4444);
		Assets.img_game_over = g.newPixmap("img/game_over.png", PixmapFormat.ARGB4444);
		Assets.img_head_down = g.newPixmap("img/head_down.png", PixmapFormat.ARGB4444);
		Assets.img_head_left = g.newPixmap("img/head_left.png", PixmapFormat.ARGB4444);
		Assets.img_head_right = g.newPixmap("img/head_right.png", PixmapFormat.ARGB4444);
		Assets.img_head_up = g.newPixmap("img/head_up.png", PixmapFormat.ARGB4444);
		Assets.img_help_1 = g.newPixmap("img/help_1.png", PixmapFormat.RGB565);
		Assets.img_help_2 = g.newPixmap("img/help_2.png", PixmapFormat.RGB565);
		Assets.img_help_3 = g.newPixmap("img/help_3.png", PixmapFormat.RGB565);
		Assets.img_main_menu = g.newPixmap("img/main_menu.png", PixmapFormat.ARGB4444);
		Assets.img_main_screen = g.newPixmap("img/main_screen.png", PixmapFormat.ARGB4444);
		Assets.img_num_0 = g.newPixmap("img/num_0.png", PixmapFormat.ARGB4444);
		Assets.img_num_1 = g.newPixmap("img/num_1.png", PixmapFormat.ARGB4444);
		Assets.img_num_2 = g.newPixmap("img/num_2.png", PixmapFormat.ARGB4444);
		Assets.img_num_3 = g.newPixmap("img/num_3.png", PixmapFormat.ARGB4444);
		Assets.img_num_4 = g.newPixmap("img/num_4.png", PixmapFormat.ARGB4444);
		Assets.img_num_5 = g.newPixmap("img/num_5.png", PixmapFormat.ARGB4444);
		Assets.img_num_6 = g.newPixmap("img/num_6.png", PixmapFormat.ARGB4444);
		Assets.img_num_7 = g.newPixmap("img/num_7.png", PixmapFormat.ARGB4444);
		Assets.img_num_8 = g.newPixmap("img/num_8.png", PixmapFormat.ARGB4444);
		Assets.img_num_9 = g.newPixmap("img/num_9.png", PixmapFormat.ARGB4444);
		Assets.img_num_dot = g.newPixmap("img/num_dot.png", PixmapFormat.ARGB4444);
		Assets.img_planet_1 = g.newPixmap("img/planet_1.png", PixmapFormat.ARGB4444);
		Assets.img_planet_2 = g.newPixmap("img/planet_2.png", PixmapFormat.ARGB4444);
		Assets.img_planet_3 = g.newPixmap("img/planet_3.png", PixmapFormat.ARGB4444);
		Assets.img_sound_off = g.newPixmap("img/sound_off.png", PixmapFormat.ARGB4444);
		Assets.img_sound_on = g.newPixmap("img/sound_on.png", PixmapFormat.ARGB4444);
		Assets.img_tail = g.newPixmap("img/tail.png", PixmapFormat.ARGB4444);
		
		Assets.snd_ate = a.newSound("snd/ate.ogg");
		Assets.snd_click = a.newSound("snd/click.ogg");
		Assets.snd_died = a.newSound("snd/died.ogg");
		Assets.snd_loop_menu_background = a.newMusic("snd/menu_background.m4a");
		
		Assets.fnt_arcade = g.newTypeface("fnt/ArcadeClassic.ttf");

		Settings.getInstance().load(game.getFileIO());
		
		game.setScreen(new MainMenuScreen(game));
	}

}
