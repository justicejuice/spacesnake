package name.tlink.android.games.spacesnake;

/**
 * Represents the {@link Planet} in the game, which will be eaten by {@link Snake}.
 * 
 * @author Timon Link <a href="mailto:timon.link@gmail.com">timon.link@gmail.com</a>
 * @date 25.08.2014
 * @version 1.0
 *
 */
public class Planet {
	/**
	 * The green planet.
	 */
	public static final int PLANET_TYPE_1 = 1;
	
	/**
	 * The purple planet.
	 */
	public static final int PLANET_TYPE_2 = 2;
	
	/**
	 * The red planet.
	 */
	public static final int PLANET_TYPE_3 = 3;
	
	/**
	 * The current planet-type green, purple or red.
	 */
	int type;
	
	/**
	 * X position of {@link Planet} in the world.
	 */
	int x;
	
	/**
	 * Y position of {@link Planet} in the world.
	 */
	int y;
	
	/**
	 * Creates a new Planet of specified type on position (x,y)
	 * @param type
	 * 	The type of the new {@link Planet}.
	 * @param x
	 * 	The x-position.
	 * @param y
	 * 	The y-position.
	 */
	public Planet(int type, int x, int y) {
		this.type = type;
		this.x = x;
		this.y = y;
	}
	
}
