package name.tlink.android.games.spacesnake;

/**
 * Represents a part of the {@link Snake} object.
 * 
 * @author Timon Link <a href="mailto:timon.link@gmail.com">timon.link@gmail.com</a>
 * @date 25.08.2014
 * @version 1.0
 *
 */
public class SnakePart {
	/**
	 * X position of the {@link SnakePart} in the {@link World}.
	 */
	int x;
	
	/**
	 * Y position of the {@link SnakePart} in {@link World}.
	 */
	int y;

	/**
	 * Creates a new {@link SnakePart} at position (x,y).
	 * @param x
	 * 	The x-position.
	 * @param y
	 * 	The y-position.
	 */
	public SnakePart(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}
}
