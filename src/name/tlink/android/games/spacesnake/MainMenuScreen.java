package name.tlink.android.games.spacesnake;

import java.util.List;

import name.tlink.android.game.framework.Game;
import name.tlink.android.game.framework.Graphics;
import name.tlink.android.game.framework.Input.TouchEvent;
import name.tlink.android.game.framework.Music;
import name.tlink.android.game.framework.Screen;

/**
 * This {@link Screen} shows the main menu to the user where he can decide
 * to start the game, list highscores or to quit.
 * 
 * @author Timon Link <a href="mailto:timon.link@gmail.com">timon.link@gmail.com</a>
 * @date 25.08.2014
 * @version 1.0
 */
public class MainMenuScreen extends Screen {
	/**
	 * The background music of the Screen.
	 */
	Music bgSound = Assets.snd_loop_menu_background;
	SpaceButton btnPlay;
	SpaceButton btnSettings;
	SpaceButton btnHighscores;
	SpaceButton btnHelp;
	Graphics graphics;

	/**
	 * Creates the Screen.
	 * @param game
	 * 	The game instance.
	 */
	public MainMenuScreen(Game game)  {
		super(game);
		graphics = game.getGraphics();
		initButtons();
	}
	
	private void initButtons() {
		int x = graphics.getWidth() / 2;
		int width = 180;
		int height = 80;
		graphics.drawLine(x, 10, width, 10, Assets.COLOR_YELLOW);
		btnPlay = new SpaceButton(graphics, "play", x, 10, width -5, height);
		btnHelp = new SpaceButton(graphics, "help", x + width+5, 10, width-5, height);
		btnSettings = new SpaceButton(graphics, "Settings", x, height + 20, 2 * width, height);
		btnHighscores = new SpaceButton(graphics, "Highscores", x, 2 * height + 30, 2*width, height);
		
		btnPlay.setOnClickAction(new MButtonListener() {
			@Override
			public void onClick() {
				game.setScreen(new GameScreen(game));
				playClick();
			}
		});
		
		btnHighscores.setOnClickAction(new MButtonListener() {
			@Override
			public void onClick() {
				game.setScreen(new HighscoreScreen(game));
				playClick();
			}
		});
		
		btnHelp.setOnClickAction(new MButtonListener() {
			@Override
			public void onClick() {
				game.setScreen(new HelpScreen(game));
				playClick();
			}
		});
		
		btnSettings.setOnClickAction(new MButtonListener() {
			@Override
			public void onClick() {
				game.setScreen(new SettingsScreen(game));
				playClick();
			}
		});
	}
	
	/**
	 * Checks if a given {@link TouchEvent} occured in a given rectangle.
	 * @param event
	 * 	The {@link TouchEvent} wich occured.
	 * @param x
	 * 	The x-position of the top left corner of the rectangle.
	 * @param y
	 * 	The y-position of the top left corner of the rectangle.
	 * @param width
	 * 	The width of the rectangle.
	 * @param height
	 * 	The height of the rectangle.
	 * @return
	 * 	true if the {@link TouchEvent} event occured inside the given rectangle.
	 */
	private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
		if (event.x > x && event.x < x + width - 1 && event.y > y && event.y < y + height -1)
			return true;
		else
			return false;
	}

	@Override
	public void dispose() {

	}

	/**
	 * Stops the background music and saves {@link Settings}.
	 */
	@Override
	public void pause() {
		Settings.getInstance().save(game.getFileIO());
		if (bgSound.isPlaying())
			bgSound.pause();
	}

	/**
	 * Draws the Screen.
	 */
	@Override
	public void present(float arg0) {
		graphics.drawPixmap(Assets.img_main_screen, 0, 0);
		//g.drawPixmap(Assets.img_main_menu, g.getWidth() / 2, 0);
		btnPlay.draw();
		btnSettings.draw();
		btnHighscores.draw();
		btnHelp.draw();
		// draw sound button:
		if (Settings.getInstance().isSoundEnabled()) {
			graphics.drawPixmap(Assets.img_sound_on, graphics.getWidth() - 120, graphics.getHeight() - 120);
		} else {
			graphics.drawPixmap(Assets.img_sound_off, graphics.getWidth() - 120, graphics.getHeight() - 120);
		}
	}

	/**
	 * Resumes the game and also the background music if sound is enabled.
	 */
	@Override
	public void resume() {
		if (!bgSound.isLooping())
			bgSound.setLooping(true);
		if (!bgSound.isPlaying() && Settings.getInstance().isSoundEnabled())
			bgSound.play();
	}
	
	/**
	 * plays the click if sound is enabled.
	 */
	private void playClick() {
		if (Settings.getInstance().isSoundEnabled())
			Assets.snd_click.play(1);
	}

	/**
	 * Updates the {@link Screen} which means it chekcs if the user
	 * pressed a menu entry and starts the selected one.
	 */
	@Override
	public void update(float arg0) {
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		// needed to clear buffer.
		game.getInput().getKeyEvents();
		int len = touchEvents.size();
		
		 
				
		if (Settings.getInstance().isSoundEnabled() && !bgSound.isPlaying()) {
			bgSound.play();
		}
		if (!Settings.getInstance().isSoundEnabled() && bgSound.isPlaying()) {
			bgSound.pause();
		}
		
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type == TouchEvent.TOUCH_UP) {
				// Toggle Sound 
				if (inBounds(event, graphics.getWidth() - 120, graphics.getHeight() - 120, 100, 100)) {
					Settings.getInstance().setSoundEnabled(!Settings.getInstance().isSoundEnabled());
					playClick();
					return;
				}
				// Start Game
				if (btnPlay.click(event))
					return;
				// Start Highscore
				if (btnHighscores.click(event))
					return;
				// Start HelpScreen
				if (btnHelp.click(event))
					return;
				//Start settings screen
				if (btnSettings.click(event))
					return;
				
			}
		}
		
	}

}
