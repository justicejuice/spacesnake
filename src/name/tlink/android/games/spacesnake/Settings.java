package name.tlink.android.games.spacesnake;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.util.Log;
import name.tlink.android.game.framework.FileIO;
import name.tlink.android.game.framework.Game;

/**
 * This class manages all user settings.
 * @author Timon Link <a href="mailto:timon.link@gmail.com">timon.link@gmail.com</a>
 * @date 25.08.2014
 * @version 1.0
 */
public class Settings {
	public static final int CONTROLL_CLASSIC = 0;
	public static final int CONTROLL_INTUITIV = 1;
	public static final int CONTROLL_WEIRD = 2;
	private static final String TAG = "SETTINGS";
	private static Settings instance;
	private static final String FILE_NAME = "spacesnake.conf";
	private static final String DIR_NAME = "Spacesnake";
	
	/**
	 * This flag says, if the sound is enabled or not.
	 */
	private boolean isSoundEnabled;
	
	private int controllMode;
	
	/**
	 * Contains the five best archieved scores.
	 */
	private List<Highscore> highscores;
	
	private Settings() {
		isSoundEnabled = true;
		highscores = new ArrayList<Highscore>();
	}
	
	public static Settings getInstance() {
		if (instance == null)
			instance = new Settings();
		return instance;
	}
	
	
	private void initScores() {
		highscores.add(new Highscore(25, "Chris"));
		highscores.add(new Highscore(20, "Tom"));
		highscores.add(new Highscore(15, "Susan"));
		highscores.add(new Highscore(10, "Jen"));
		highscores.add(new Highscore(5, "Alex"));
	}
	
	public boolean isSoundEnabled() {
		return this.isSoundEnabled;
	}
	
	public void setSoundEnabled(boolean bool) {
		this.isSoundEnabled = bool;
	}
	
	public List<Highscore> getHighscores() {
		return this.highscores;
	}
	
	/**
	 * Loads the file which contains the settings.
	 * @param files
	 * 	The {@link FileIO} instance of current {@link Game} instance.
	 */
	void load(FileIO files) {
		BufferedReader in = null;
		if (this.highscores.size() == 0)
			initScores();
		try {
			in = new BufferedReader(new InputStreamReader( files.readFile(DIR_NAME + File.separator +  FILE_NAME)));
			isSoundEnabled = Boolean.parseBoolean(in.readLine());
			
			for (Highscore highscore : highscores) {
				String line = in.readLine();
				String[] split = line.split("##");
				highscore.setName(split[0]);
				highscore.setScore(split[1]);
			}
			controllMode = Integer.parseInt(in.readLine());
		} catch (IOException e) {
		} 
		catch (ArrayIndexOutOfBoundsException e) {
			highscores.clear();
			initScores();
			Log.e(TAG, e.getLocalizedMessage());
		}
		catch (NumberFormatException e) {
			controllMode = CONTROLL_CLASSIC;
		} finally {
				try {
					if (in != null)
						in.close();
				} catch (IOException e) {
				}
		}
	}

	/**
	 * Saves the settings to file.
	 * @param files
	 * 	The {@link FileIO} instance of current {@link Game} instance.
	 */
	void save(FileIO files) {
		BufferedWriter out = null;
		
		try {
			out = new BufferedWriter(new OutputStreamWriter(files.writeFile(DIR_NAME, FILE_NAME)));
			out.write(Boolean.toString(isSoundEnabled));
			out.newLine();
			for (Highscore highscore : highscores) {
				out.write(highscore.getName() + "##" + highscore.getScore());
				out.newLine();
			}
			out.write(Integer.toString(controllMode));
			out.newLine();
		} catch (IOException e) {
		} finally {
			try {
				if (out != null) 
					out.close();
			} catch (IOException e) {
			}
		}
	}
	
	public boolean isNewHighscore(int score) {
		Highscore last = highscores.get(4);
		if (last.getScore() < score)
			return true;
		else
			return false;
	}
	
	public int getControllMode() {
		return controllMode;
	}
	
	public void setControllMode(int mode) {
		this.controllMode = mode;
	}
	
	/**
	 * Adds a new highscore at the corresponding position in highscores array.
	 * @param score
	 * 	The new highscore to add.
	 * @param
	 * 	The name of the player.
	 * 
	 */
	public void addScore(int score, String name) {
		highscores.get(4).setScore(score);
		highscores.get(4).setName(name);
		Collections.sort(highscores);
	}
}
