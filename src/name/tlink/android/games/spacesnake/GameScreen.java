package name.tlink.android.games.spacesnake;

import java.util.List;
import name.tlink.android.game.framework.Game;
import name.tlink.android.game.framework.Graphics;
import name.tlink.android.game.framework.Input.TouchEvent;
import name.tlink.android.game.framework.Pixmap;
import name.tlink.android.game.framework.Screen;

/**
 * This {@link Screen} is where the actual game is played.
 * It defines three types of state:
 * 	<ol>
 * 	<li> Running </li>
 * 	<li> Paused </li>
 * 	<li> GameOver </li>
 * </ol>
 * Running updates the game logic and redraws the world.
 * Paused shows a pause menu to the user and also pauses the game logic.
 * GameOver indicates that the game is over and shows a message to user.
 * 
 * @author Timon Link <a href="mailto:timon.link@gmail.com">timon.link@gmail.com</a>
 * @date 25.08.2014
 * @version 1.0
 */
public class GameScreen extends Screen {
	
	private String username = "user";
	/**
	 * Left button pressed.
	 */
	public static final int BTN_LEFT = 0;
	
	/**
	 * Right button pressed.
	 */
	public static final int BTN_RIGHT = 1;
	
	/**
	 * Contains the three states of {@link GameScreen}.
	 * @see GameScreen
	 * @author Timon Link <a href="mailto:timon.link@gmail.com">timon.link@gmail.com</a>
	 * @date 25.08.2014
	 * @version 1.0
	 */
	enum GameState { Running, Paused, GameOver };
	
	/**
	 * Current state of the game.
	 */
	GameState state = GameState.Running;
	
	/**
	 * The world where the snake lives.
	 */
	World world;
	
	/**
	 * The score before the snake ate a planet.
	 */
	int oldScore = 0;
	
	/**
	 * Current score to print to the screen.
	 */
	String score = "0";
	
	/**
	 * Creates a new {@link GameScreen}.
	 * @param game
	 * 	The game instance.
	 */
	public GameScreen(Game game) {
		super(game);
		world = new World();
	}

	@Override
	public void dispose() {

	}

	/**
	 * Pauses the game and saves the archieved score if
	 * the game is over.
	 */
	@Override
	public void pause() {
		if (state == GameState.Running)
			state = GameState.Paused;
	}

	/**
	 * Presents the current {@link GameScreen}. Decides
	 * on {@link GameState} which UI must be drawn.
	 */
	@Override
	public void present(float deltaTime) {
		Graphics g = game.getGraphics();
		
		g.drawPixmap(Assets.img_background, 0, 0);
		
		drawWorld(g, world);
		
		if (state == GameState.Running)
			drawRunningUI(g);
		
		if (state == GameState.Paused)
			drawPausedUI(g);
		
		if (state == GameState.GameOver)
			drawGameOverUI(g);
		
	}

	@Override
	public void resume() {
	
	}

	/**
	 * Updates the game logic on current {@link GameScreen}.
	 * Decides on {@link GameState} which state must be
	 * updated.
	 */
	@Override
	public void update(float deltaTime) {
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		game.getInput().getKeyEvents();
		if (state == GameState.Running)
			updateRunning(touchEvents, deltaTime);
		else if (state == GameState.Paused)
			updatePaused(touchEvents);
		else if (state == GameState.GameOver)
			updateGameOver(touchEvents);
		
	}
	
	/**
	 * Draws the score to the screen.
	 * @param g
	 * 	The {@link Graphics} instance.
	 * @param x
	 * 	The x-position.
	 * @param y
	 * 	The y-position.
	 */
	private void drawScore(Graphics g, int x, int y) {
		g.drawText(score, x, y, Assets.COLOR_YELLOW, Assets.fnt_arcade, 50f);
	}
	
	/**
	 * Draws the UI of {@link GameState} Running.
	 * 
	 * @param g
	 * The {@link Graphics} instance which does the drawing.
	 */
	private void drawRunningUI (Graphics g) {
		int yCenter = g.getHeight() / 2 - Assets.img_arrow_right.getHeight() / 2;
		int frameRightPos = Assets.img_arrow_right.getWidth() + 12 + Assets.img_frame_right.getWidth();
		int frameLeftPos =  Assets.img_arrow_left.getWidth() + 12;
		
		g.drawPixmap(Assets.img_arrow_left, 6, yCenter);
		g.drawPixmap(Assets.img_frame_left, frameLeftPos, 0);
		g.drawPixmap(Assets.img_frame_right, g.getWidth() - frameRightPos, 0);
		g.drawPixmap(Assets.img_arrow_right, g.getWidth() - (Assets.img_arrow_right.getWidth() + 6), yCenter);
		drawScore(g, 10, 50);
	}
	
	/**
	 * Draws the UI of {@link GameState} Paused.
	 * @param g
	 * The {@link Graphics} instance which does the drawing.
	 */
	private void drawPausedUI(Graphics g) {
		Pixmap menu = Assets.img_game_menu;
		int yCenter = g.getHeight() / 2 - menu.getHeight() / 2;
		int xCenter = g.getWidth() / 2 - menu.getWidth() / 2;
		g.drawPixmap(menu, xCenter, yCenter);		
	}
	
	/**
	 * Draws the UI of {@link GameScreen} GameOver.
	 * @param g
	 * 	The {@link Graphics} instance which does the drawing
	 */
	private void drawGameOverUI(Graphics g) {
		Pixmap gameOver = Assets.img_game_over;
		
		int centerX = g.getWidth() / 2 - gameOver.getWidth() / 2;
		int centerY = g.getHeight() / 2 - gameOver.getHeight() / 2;
		
		g.drawPixmap(gameOver, centerX, centerY);
		g.drawText("SCORE: " + world.score, centerX, centerY + gameOver.getHeight() + 50, 
					Assets.COLOR_YELLOW, Assets.fnt_arcade, 50f);
		
	}
	
	/**
	 * This method draws the current state of the world in which
	 * the game is happening.
	 * @param g
	 * 	The {@link Graphics} instance which does the drawing.
	 * @param world
	 * 	The world where the game happens.
	 */
	private void drawWorld(Graphics g, World world) {
		Snake snake = world.snake;
		SnakePart head = snake.parts.get(0);
		Planet planet = world.planet;
		Pixmap planetPixmap = null;
		
		switch (planet.type) {
		case Planet.PLANET_TYPE_1:
			planetPixmap = Assets.img_planet_1;
			break;
		case Planet.PLANET_TYPE_2:
			planetPixmap = Assets.img_planet_2;
			break;
		case Planet.PLANET_TYPE_3:
			planetPixmap = Assets.img_planet_3;
		default:
			planetPixmap = Assets.img_planet_3;
		}
		
		int x = planet.x * World.WORLD_PIXEL_SIZE;
		int y = planet.y * World.WORLD_PIXEL_SIZE;
		
		g.drawPixmap(planetPixmap, x + 112, y);
		
		for (int i = 1; i < snake.parts.size(); i++) {
			SnakePart part = snake.parts.get(i);
			x = part.x * World.WORLD_PIXEL_SIZE + 3;
			y = part.y * World.WORLD_PIXEL_SIZE + 3;
			g.drawPixmap(Assets.img_tail, x + 112, y);
		}
		
		Pixmap headPixmap = Assets.img_head_right;
		
		if (snake.direction == Snake.DIR_UP) {
			headPixmap = Assets.img_head_up;
			x = head.x * World.WORLD_PIXEL_SIZE;
			y = head.y * World.WORLD_PIXEL_SIZE - 3;
		}
		if (snake.direction == Snake.DIR_DOWN) {
			headPixmap = Assets.img_head_down;
			x = head.x * World.WORLD_PIXEL_SIZE;
			y = head.y * World.WORLD_PIXEL_SIZE + 3;
		}
		if (snake.direction == Snake.DIR_LEFT) {
			headPixmap = Assets.img_head_left;
			x = head.x * World.WORLD_PIXEL_SIZE - 3;
			y = head.y * World.WORLD_PIXEL_SIZE;
		}
		if (snake.direction == Snake.DIR_RIGHT) {
			headPixmap = Assets.img_head_right;
			x = head.x * World.WORLD_PIXEL_SIZE + 3;
			y = head.y * World.WORLD_PIXEL_SIZE;
		}

		g.drawPixmap(headPixmap, x + 112, y);
	}
	
	/**
	 * Checks if a {@link TouchEvent} happens on the game board where
	 * the {@link Snake} moves.
	 * @param event
	 * 	The {@link TouchEvent} which happened.
	 * @return
	 * 	true if the event occured on the game board, false otherwise.
	 */
	private boolean isOnBoard(TouchEvent event) {
		int width = game.getGraphics().getWidth();
		int height = game.getGraphics().getHeight();
		
		if (event.x > Assets.img_arrow_left.getWidth() + 12 && 
				event.x < width - (Assets.img_arrow_right.getWidth() + 12) &&
				event.y > 0 && event.y < height)
			return true;
		
		else return false;
	}
	
	private void controllSnake(int button) {
		int mode = Settings.getInstance().getControllMode();
		if (mode == Settings.CONTROLL_CLASSIC) {
			if (button == BTN_LEFT)
				world.snake.turnLeft();
			if (button == BTN_RIGHT)
				world.snake.turnRight();
		}
		if (mode == Settings.CONTROLL_INTUITIV) {
			if (button == BTN_LEFT) {
				if (world.snake.direction == Snake.DIR_DOWN)
					world.snake.turnRight();
				else
					world.snake.turnLeft();
			}
			if (button == BTN_RIGHT) {
				if (world.snake.direction == Snake.DIR_DOWN)
					world.snake.turnLeft();
				else
					world.snake.turnRight();
			}
			
		}
		if (mode == Settings.CONTROLL_WEIRD) {
			if (button == BTN_LEFT)
				world.snake.turnRight();
			if (button == BTN_RIGHT)
				world.snake.turnLeft();
		}
	}
	
	/**
	 * Updates the world because the {@link GameState} is Running and
	 * the {@link World} changes.
	 * @param touchEvents
	 * 	All {@link TouchEvent}s which occured.
	 * @param deltaTime
	 * 	The current time minus the time of last shown frame.
	 */
	private void updateRunning(List<TouchEvent> touchEvents, float deltaTime) {
		for (TouchEvent touchEvent : touchEvents) {
			if (touchEvent.type == TouchEvent.TOUCH_UP) {
				if (isOnBoard(touchEvent)) {
					playClick();
					state = GameState.Paused;
					return;
				}
			}
			
			if (touchEvent.type == TouchEvent.TOUCH_DOWN) {
				if (isButtonPressed(touchEvent, BTN_LEFT)) {
					controllSnake(BTN_LEFT);
					playClick();
				}
				if (isButtonPressed(touchEvent, BTN_RIGHT)) {
					controllSnake(BTN_RIGHT);
					playClick();
				}
			}
		}
		
		world.update(deltaTime);
		
		if (world.gameOver) {
			if (Settings.getInstance().isSoundEnabled())
				Assets.snd_died.play(1);
			state = GameState.GameOver;
		}
		
		if (oldScore != world.score) {
			oldScore = world.score;
			score = String.valueOf(oldScore);
			if (Settings.getInstance().isSoundEnabled())
				Assets.snd_ate.play(1);
		}
	}
	
	/**
	 * Updates the game logic of {@link GameState} Paused.
	 * @param touchEvents
	 * 	The {@link TouchEvent}s which occured.
	 */
	private void updatePaused(List<TouchEvent> touchEvents) {
		Graphics g = game.getGraphics();
		Pixmap menu = Assets.img_game_menu;
		int menuX = g.getWidth() / 2 - menu.getWidth() / 2;
		int menuY = g.getHeight() / 2 - menu.getHeight() / 2;
		
		for (TouchEvent event : touchEvents) {
			
			if (event.type == TouchEvent.TOUCH_UP) {
				
				if (isInBounds(event, menuX, menuY, menu.getWidth(), menu.getHeight()/2)) {
					playClick();
					state = GameState.Running;
					return;
				}
				
				if (isInBounds(event, menuX, menuY + menu.getHeight() / 2, menu.getWidth(), menu.getHeight() / 2)) {
					playClick();
					game.setScreen(new MainMenuScreen(game));
					return;
				}
				
			}
			
		}
	}
	
	/**
	 * Updates the game logic of {@link GameState} GameOver.
	 * @param touchEvents
	 * 	The {@link TouchEvent}s which occured.
	 */
	private void updateGameOver(List<TouchEvent> touchEvents) {
		Graphics g = game.getGraphics();
		int imgWidth = Assets.img_game_over.getWidth();
		int imgHeight = Assets.img_game_over.getHeight();
		int centerX = g.getWidth() / 2 -  imgWidth / 2;
		int centerY = g.getHeight() / 2 -  imgHeight/ 2;
		
		for (TouchEvent event : touchEvents) {
			if (event.type == TouchEvent.TOUCH_UP) {
				if (isInBounds(event, centerX, centerY, imgWidth, imgHeight)) {
					playClick();
					if (Settings.getInstance().isNewHighscore(world.score)) {
						Settings.getInstance().addScore(world.score, getUserName());
						Settings.getInstance().save(game.getFileIO());
					}
					game.setScreen(new MainMenuScreen(game));
				}
					
			}
		}
	}
	
	/**
	 * Decides on {@link Settings}.isSoundEnabled, if the click sound
	 * has to be played. If yes, it plays it.
	 */
	private void playClick() {
		if (Settings.getInstance().isSoundEnabled())
			Assets.snd_click.play(1);
	}
	
	/**
	 * Checks if the {@link TouchEvent} occured on a button identified
	 * by parameter which.
	 * @param event
	 * 	The {@link TouchEvent} which occured.
	 * @param which
	 * 	This parameter should be BTN_LEFT or BTN_RIGHT of class {@link GameScreen}.
	 * @return
	 * 	true if one of the buttons was pressed, false otherwise.
	 */
	private boolean isButtonPressed(TouchEvent event, int which) {
		Graphics g = game.getGraphics();
		int btn_width = Assets.img_arrow_left.getWidth();
		int btn_height = Assets.img_arrow_left.getHeight();
		int btn_x;
		int btn_y = g.getHeight() / 2 - btn_width / 2;
		
		if (which == BTN_LEFT) 
			btn_x = 6;
		else
			btn_x = g.getWidth() - (Assets.img_arrow_right.getWidth() + 6);
		
		if (event.x > btn_x && event.x < btn_x + btn_width && event.y > btn_y && event.y < btn_y + btn_height)
			return true;
		else
			return false;
		
	}
	
	private String getUserName() {	
		username = game.showInputDialog("new Highscore!", "Please enter your name", "OK", "No thanks", "user");
		return this.username.replace(" ", "");
	}
	
	/**
	 * Checks if a given {@link TouchEvent} occured in a specified rectangle.
	 * @param event
	 * 	The {@link TouchEvent} which occured.
	 * @param x
	 * 	The x position of top-left corner of the rectangle.
	 * @param y
	 * 	The y position of top-left corner of the rectangle.
	 * @param width
	 *  The width of the rectangle.
	 * @param height
	 * 	The height of the rectangle
	 * @return
	 * 	true if the {@link TouchEvent} occured in the rectangle, false otherwise.
	 */
	private boolean isInBounds(TouchEvent event, int x, int y, int width, int height) {		
		if (event.x > x && event.x < x + width && event.y > y && event.y < y + height)
			return true;
		else
			return false;
	}

}
