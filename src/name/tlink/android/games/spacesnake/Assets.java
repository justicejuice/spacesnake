package name.tlink.android.games.spacesnake;

import android.graphics.Color;
import android.graphics.Typeface;
import name.tlink.android.game.framework.Graphics;
import name.tlink.android.game.framework.Pixmap;
import name.tlink.android.game.framework.Music;
import name.tlink.android.game.framework.Sound;

/**
 * Contains all references to needed game asssets.
 * @author Timon Link <a href="mailto:timon.link@gmail.com">timon.link@gmail.com</a>
 * @date 25.08.2014
 * @version 1.0
 */
public class Assets {
	static Pixmap img_arrow_left;
	static Pixmap img_arrow_right;
	static Pixmap img_background;
	static Pixmap img_frame_left;
	static Pixmap img_frame_right;
	static Pixmap img_game_menu;
	static Pixmap img_game_over;
	static Pixmap img_head_down;
	static Pixmap img_head_left;
	static Pixmap img_head_right;
	static Pixmap img_head_up;
	static Pixmap img_help_1;
	static Pixmap img_help_2;
	static Pixmap img_help_3;
	static Pixmap img_main_menu;
	static Pixmap img_main_screen;
	static Pixmap img_num_0;
	static Pixmap img_num_1;
	static Pixmap img_num_2;
	static Pixmap img_num_3;
	static Pixmap img_num_4;
	static Pixmap img_num_5;
	static Pixmap img_num_6;
	static Pixmap img_num_7;
	static Pixmap img_num_8;
	static Pixmap img_num_9;
	static Pixmap img_num_dot;
	static Pixmap img_planet_1;
	static Pixmap img_planet_2;
	static Pixmap img_planet_3;
	static Pixmap img_sound_on;
	static Pixmap img_sound_off;
	static Pixmap img_tail;

	static Sound snd_ate;
	static Sound snd_click;
	static Sound snd_died;
	static Music snd_loop_game_background;
	static Music snd_loop_menu_background;
	
	static Typeface fnt_arcade;
	
	static final int COLOR_YELLOW = Color.rgb(246, 255, 0);

	/**
	 * Draws a given number-string to the screen.
	 * @param g
	 * 	The {@link Graphics} instance which does the drawing work.
	 * @param line
	 * 	The number-string to draw.
	 * @param x
	 * 	The x-position of top left corner of the area to draw to.
	 * @param y
	 * 	The y-position of top left corner of the area to draw to.
	 */
	static void drawNumbers(Graphics g, String line, int x, int y) {
		int len = line.length();
		//x = x - (len * 30) / 2;
		Pixmap toDraw;
		for (int i = 0; i < len; i++) {
			int num = (line.charAt(i) - '0');
			switch (num) {
			case 0:
				toDraw = Assets.img_num_0;
				break;
			case 1:
				toDraw = Assets.img_num_1;
				break;
			case 2:
				toDraw = Assets.img_num_2;
				break;
			case 3:
				toDraw = Assets.img_num_3;
				break;
			case 4:
				toDraw = Assets.img_num_4;
				break;
			case 5:
				toDraw = Assets.img_num_5;
				break;
			case 6:
				toDraw = Assets.img_num_6;
				break;
			case 7:
				toDraw = Assets.img_num_7;
				break;
			case 8:
				toDraw = Assets.img_num_8;
				break;
			case 9:
				toDraw = Assets.img_num_9;
				break;
			default:
				toDraw = Assets.img_num_dot;
				break;
			}
			g.drawPixmap(toDraw, x, y);
			x += 30;
		}
	}

}
