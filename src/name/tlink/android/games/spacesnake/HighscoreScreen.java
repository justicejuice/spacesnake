package name.tlink.android.games.spacesnake;

import java.util.List;

import name.tlink.android.game.framework.Game;
import name.tlink.android.game.framework.Graphics;
import name.tlink.android.game.framework.Music;
import name.tlink.android.game.framework.Screen;
import name.tlink.android.game.framework.Input.TouchEvent;

/**
 * This class presents the hichgscore to 
 * the user.
 * @author Timon Link <a href="mailto:timon.link@gmail.com">timon.link@gmail.com</a>
 * @date 25.08.2014
 * @version 1.0
 */
public class HighscoreScreen extends Screen {

	/**
	 * The background music which is played.
	 */
	Music bgSound = Assets.snd_loop_menu_background;
	
	/**
	 * All lines of the highscores in the stored settings file.
	 */
	String [] lines = new String[5];
	
	/**
	 * Contains the coordinates of the back button.
	 * at index 0 it's the x-position at index 1 it's
	 * the y-position.
	 */
	int [] btn_back_pos = new int[2];
	
	/**
	 * Creates a new instance of the highscore screen.
	 * @param game
	 * 	The current {@link Game} instance.
	 */
	public HighscoreScreen(Game game) {
		super(game);
		Settings s = Settings.getInstance();
		for (int i = 0; i < lines.length; i++) {
			lines[i] = s.getHighscores().get(i).getPrintScore() + " " + s.getHighscores().get(i).getName();
		}
		btn_back_pos[0] = game.getGraphics().getWidth()/2 - Assets.img_arrow_left.getWidth()/2;
		btn_back_pos[1] = game.getGraphics().getHeight() - Assets.img_arrow_left.getHeight();
	}

	@Override
	public void dispose() {

	}


	@Override
	public void pause() {
		if (bgSound.isPlaying())
			bgSound.pause();
	}

	/**
	 * Draws the current {@link HighscoreScreen}.
	 */
	@Override
	public void present(float arg0) {
		Graphics g = game.getGraphics();
		int highscore_pos = g.getWidth() / 2 - Assets.img_main_menu.getWidth() /2;
		g.drawPixmap(Assets.img_background, 0, 0);
		g.drawPixmap(Assets.img_main_menu, highscore_pos, 0, 0, 80, 400, 80);
		int y = 100;
		int x = g.getWidth()/2 - Assets.img_main_menu.getWidth() / 2 + 30;
		for (int i = 0; i < lines.length; i++) {
			y += 40;
			g.drawText(lines[i], x, y, Assets.COLOR_YELLOW, Assets.fnt_arcade, 52f);
		}
		g.drawPixmap(Assets.img_arrow_left, btn_back_pos[0], btn_back_pos[1]);
	}

	@Override
	public void resume() {
		if (!bgSound.isLooping())
			bgSound.setLooping(true);
		if (Settings.getInstance().isSoundEnabled()&& !bgSound.isPlaying())
			bgSound.play();
	}

	/**
	 * Updates the screen which means it starts a new instance of {@link MainMenuScreen} if the
	 * user touches the back button.
	 */
	@Override
	public void update(float arg0) {
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		game.getInput().getKeyEvents();
		
		for (TouchEvent event : touchEvents) {
			if (event.type == TouchEvent.TOUCH_UP) {
				if (inBounds(event, btn_back_pos[0], btn_back_pos[1], 
						Assets.img_arrow_left.getHeight(), Assets.img_arrow_left.getHeight())) {
					game.setScreen(new MainMenuScreen(game));
					if (Settings.getInstance().isSoundEnabled())
						Assets.snd_click.play(1);
					return;
				}
			}
		}
	}
	
	/**
	 * Checks if a given {@link TouchEvent} occured in a given rectangle.
	 * @param event
	 * 	The {@link TouchEvent} wich occured.
	 * @param x
	 * 	The x-position of the top left corner of the rectangle.
	 * @param y
	 * 	The y-position of the top left corner of the rectangle.
	 * @param width
	 * 	The width of the rectangle.
	 * @param height
	 * 	The height of the rectangle.
	 * @return
	 * 	true if the {@link TouchEvent} event occured inside the given rectangle.
	 */
	private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
		if (event.x > x && event.x < x + width - 1 && event.y > y && event.y < y + height -1)
			return true;
		else
			return false;
	}

}
