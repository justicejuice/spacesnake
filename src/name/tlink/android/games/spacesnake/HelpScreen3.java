package name.tlink.android.games.spacesnake;

import java.util.List;

import name.tlink.android.game.framework.Game;
import name.tlink.android.game.framework.Graphics;
import name.tlink.android.game.framework.Music;
import name.tlink.android.game.framework.Screen;
import name.tlink.android.game.framework.Input.TouchEvent;

/**
 * This class presents the last help screen to the user.
 * @author Timon Link <a href="mailto:timon.link@gmail.com">timon.link@gmail.com</a>
 * @date 25.08.2014
 * @version 1.0
 */
public class HelpScreen3 extends Screen {
	
	/**
	 * The background music which is played.
	 */
	Music bgSound = Assets.snd_loop_menu_background;

	/**
	 * Creates an instance of {@link HelpScreen3}.
	 * @param game
	 * 	The current {@link Game} instance.
	 */
	public HelpScreen3(Game game) {
		super(game);
	}

	@Override
	public void dispose() {

	}

	@Override
	public void pause() {
		if (bgSound.isPlaying())
			bgSound.pause();

	}

	@Override
	public void present(float arg0) {
		Graphics g = game.getGraphics();
		g.drawPixmap(Assets.img_help_3, 0, 0);

	}

	@Override
	public void resume() {
		if (!bgSound.isLooping())
			bgSound.setLooping(true);
		if (Settings.getInstance().isSoundEnabled() && !bgSound.isPlaying()) 
			bgSound.play();

	}

	/**
	 * Checks if a given {@link TouchEvent} occured in a given rectangle.
	 * @param event
	 * 	The {@link TouchEvent} wich occured.
	 * @param x
	 * 	The x-position of the top left corner of the rectangle.
	 * @param y
	 * 	The y-position of the top left corner of the rectangle.
	 * @param width
	 * 	The width of the rectangle.
	 * @param height
	 * 	The height of the rectangle.
	 * @return
	 * 	true if the {@link TouchEvent} event occured inside the given rectangle.
	 */
	private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
		if (event.x > x && event.x < x + width - 1 && event.y > y && event.y < y + height -1)
			return true;
		else
			return false;
	}
	
	/**
	 * Updates the screen which means it starts a new instance of {@link MainMenuScreen} if the
	 * user touches the screen.
	 */
	@Override
	public void update(float arg0) {
		Graphics g = game.getGraphics();
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		game.getInput().getKeyEvents();
		int len = touchEvents.size();
		
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type == TouchEvent.TOUCH_UP) {
				if (inBounds(event, 0, 0, g.getWidth(), g.getHeight())) {
					game.setScreen(new MainMenuScreen(game));
					if (Settings.getInstance().isSoundEnabled())
						Assets.snd_click.play(1);
					return;
				}
			}
		}
	}

}
