package name.tlink.android.games.spacesnake;

import java.util.List;

import name.tlink.android.game.framework.Game;
import name.tlink.android.game.framework.Graphics;
import name.tlink.android.game.framework.Input.TouchEvent;
import name.tlink.android.game.framework.Music;
import name.tlink.android.game.framework.Screen;

public class SettingsScreen extends Screen {

	SpaceButton btnCancel;
	SpaceButton btnOkay;
	
	SpaceButton btnClassic;
	SpaceButton btnIntuitiv;
	SpaceButton btnWeird;
	
	ButtonGroup buttonGroup;

	Graphics graphics;
	Music bgSound = Assets.snd_loop_menu_background;

	public SettingsScreen(Game game) {
		super(game);
		this.graphics = game.getGraphics();
		initButtons();
		initButtonGroup();
	}

	private void initButtons() {
		int height = 80;
		int width = 180;
		
		this.btnCancel = new SpaceButton(graphics, "cancel", 10,
				graphics.getHeight() - (height+10), width, height);
		
		this.btnOkay = new SpaceButton(graphics, "okay", graphics.getWidth()
				- (width + 10), graphics.getHeight() - (height+10), width, height);
		

		this.btnCancel.setOnClickAction(new MButtonListener() {
			@Override
			public void onClick() {
				game.setScreen(new MainMenuScreen(game));
				playClick();
			}
		});
		
		this.btnOkay.setOnClickAction(new MButtonListener() {
			@Override
			public void onClick() {
				save();
				game.setScreen(new MainMenuScreen(game));
				playClick();
			}
		});
	}
	
	private void initButtonGroup() {
		int controllMode = Settings.getInstance().getControllMode();
		int selectedId;
		int width = 250;
		int height = 80;
		buttonGroup = new ButtonGroup(graphics, graphics.getWidth() / 2 - width/2, 100);
		
		this.btnClassic = new SpaceButton(graphics, "classic", 0, 0, width, height);
		this.btnIntuitiv = new SpaceButton(graphics, "intuitiv", 0, 0, width, height);
		this.btnWeird = new SpaceButton(graphics, "weird", 0, 0, width, height);
		
		buttonGroup.addButton(btnClassic);
		buttonGroup.addButton(btnIntuitiv);
		buttonGroup.addButton(btnWeird);
		
		switch (controllMode) {
		case Settings.CONTROLL_CLASSIC:
			selectedId = btnClassic.getId();
			break;
		case Settings.CONTROLL_INTUITIV:
			selectedId = btnIntuitiv.getId();
			break;
		case Settings.CONTROLL_WEIRD:
			selectedId = btnWeird.getId();
			break;
		default:
			selectedId = btnClassic.getId();
			break;
		}
		buttonGroup.setSelectedButton(selectedId);
		
	}
	
	private void save() {
		int id = buttonGroup.getSelectedButton().getId();
		int mode = Settings.getInstance().getControllMode();
		
		if (id == btnClassic.getId())
			mode = Settings.CONTROLL_CLASSIC;
		
		if (id == btnIntuitiv.getId())
			mode = Settings.CONTROLL_INTUITIV;
		
		if (id == btnWeird.getId())
			mode = Settings.CONTROLL_WEIRD;
		
		Settings.getInstance().setControllMode(mode);
		Settings.getInstance().save(game.getFileIO());
	}
	
	private void playClick() {
		if (Settings.getInstance().isSoundEnabled())
			Assets.snd_click.play(1);
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		if (bgSound.isPlaying())
			bgSound.pause();
	}

	@Override
	public void present(float arg0) {
		graphics.drawPixmap(Assets.img_background, 0, 0);
		btnCancel.draw();
		btnOkay.draw();
		
		graphics.drawText("controlls", 10, 60, Assets.COLOR_YELLOW, Assets.fnt_arcade, 50f);
		buttonGroup.draw(ButtonGroup.ARRANGE_VERTICAL);
		
	}

	@Override
	public void resume() {
		if (!this.bgSound.isLooping())
			bgSound.setLooping(true);
		if (Settings.getInstance().isSoundEnabled() && !bgSound.isPlaying())
			bgSound.play();
	}

	@Override
	public void update(float arg0) {
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		game.getInput().getKeyEvents();
		for (TouchEvent touchEvent : touchEvents) {
			if (touchEvent.type == TouchEvent.TOUCH_UP) {
				if (btnCancel.click(touchEvent))
					return;
				if (btnOkay.click(touchEvent))
					return;
				if (buttonGroup.checkClick(touchEvent))
					return;
			}
		}
		
	}

}
