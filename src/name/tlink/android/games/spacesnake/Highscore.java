package name.tlink.android.games.spacesnake;


public class Highscore implements Comparable<Highscore> {
	public static final int MAX_NAME = 10;
	private int score;
	private String name;
	
	public Highscore(int score, String name) {
		this.score = score;
		this.name = name;
		normalizeName();
	}
	
	public int compareTo(Highscore another) {
		if (score < another.score)
			return 1;
		else if (score > another.score)
			return -1;
		else
			return 0;
		
	}

	public int getScore() {
		return score;
	}
	
	public String getPrintScore() {
		StringBuilder sb = new StringBuilder();
		if (this.score < 10)
			sb.append("00" + score);
		else if (this.score < 100)
			sb.append("0" + score);
		else
			sb.append(this.score);
		return sb.toString();
	}

	public void setScore(String score) {
		this.score = Integer.parseInt(score);
	}
	
	public void setScore(int score) {
		this.score = score;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		normalizeName();
	}
	
	private void normalizeName() {
		StringBuilder sb = new StringBuilder();
		int len = this.name.length();
		
		if (len < MAX_NAME) {
			for (int i = 0; i < MAX_NAME - len; i++) {
				sb.append("..");
			}
			setName(sb.toString() + name);
		}
	}
	
	
	
	
}
