package name.tlink.android.games.spacesnake;

import android.graphics.Color;
import name.tlink.android.game.framework.Graphics;
import name.tlink.android.game.framework.Input.TouchEvent;

public class SpaceButton {
	Graphics graphics;
	
	String text;
	
	int x;
	int y;
	int width;
	int height;
	int txtColor = Assets.COLOR_YELLOW;
	private int id;
	boolean isActive = false;
	MButtonListener listener;
	
	
	public SpaceButton(Graphics graphics, String text, int x, int y, int width,
			int height) {
		super();
		this.graphics = graphics;
		this.text = text;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		id = System.identityHashCode(this);
	}
	
	public void draw() {
		int textWidth = text.length() * 28;
		if (isActive) {
			graphics.drawRect(x, y, width, height, Assets.COLOR_YELLOW);
		}
		else {
			graphics.drawLine(x, y, x+width, y, Assets.COLOR_YELLOW);
			graphics.drawLine(x, y, x, y+height, Assets.COLOR_YELLOW);
			graphics.drawLine(x, y+height, x+width, y+height, Assets.COLOR_YELLOW);
			graphics.drawLine(x+width, y, x+width, y+height, Assets.COLOR_YELLOW);
		}
		
		graphics.drawText(text, (x + width / 2) - textWidth / 2, y + height/2 + 15, txtColor, Assets.fnt_arcade, 50f);
	}
	
	public void setOnClickAction(MButtonListener listener) {
		this.listener = listener;
	}
	
	public boolean click(TouchEvent event) {
		if (isInBounds(event)) {
			this.listener.onClick();
			return true;
		} else {
			return false;
		}
	}
	
	private boolean isInBounds(TouchEvent event) {
		if (event.x > x && event.x < x + width - 1 && event.y > y && event.y < y + height -1)
			return true;
		else
			return false;
	}
	
	public void setActive() {
		this.txtColor = Color.BLACK;
		isActive = true;
		
	}
	
	public void setInactive() {
		this.txtColor = Assets.COLOR_YELLOW;
		isActive = false;
	}
	
	public int getId() {
		return id;
	}
	
	
}
