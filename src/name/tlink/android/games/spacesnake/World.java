package name.tlink.android.games.spacesnake;

import java.util.Random;
/**
 * This class represents the virtual world of space snake. It defines its own
 * world-coordinates and initializes the position of the {@link Snake} and
 * {@link Planet} on the game-board.
 * 
 * @author Timon Link <a href="mailto:timon.link@gmail.com">timon.link@gmail.com</a>
 * @date 25.08.2014
 * @version 1.0
 *
 */
public class World {
	/**
	 * The width of {@link World} in world-coordinate-system
	 */
	public static final int WORLD_WIDTH = 24;
	
	/**
	 * The height of {@link World} in world-coordinate-system
	 */
	public static final int WORLD_HEIGHT = 20;
	
	/**
	 * The size of an world-coordinate pixel iind real pixels.
	 */
	public static final int WORLD_PIXEL_SIZE = 24;
	
	/**
	 * The score increment, when a planet was eaten.
	 */
	public static final int SCORE_INCREMENT = 5;
	
	/**
	 * Initial time in seconds, which will be subtracted
	 * from tickTime to perform frame independent movement.
	 */
	public static final float TICK_INITIAL = 0.15f;
	
	/**
	 * TICK_INITIAL will be decremented by TICK_DECREMENT
	 * every ten planets eaten to increment speed of snake.
	 */
	public static final float TICK_DECREMENT = 0.005f;
	
	/**
	 * The {@link Snake} in the {@link World}.
	 */
	public Snake snake;
	
	/**
	 * The {@link Planet} in the {@link World}.
	 */
	public Planet planet;
	
	/**
	 * Indicates if the game is over or not.
	 */
	public boolean gameOver;
	
	/**
	 * Stores the score.
	 */
	public int score;
	
	private boolean [][] fields = new boolean[WORLD_WIDTH][WORLD_HEIGHT];
	private Random random = new Random();
	private float tickTime = 0;
	private float tick = TICK_INITIAL;
	
	/**
	 * Creates a new instance of {@link World}. It also instantiate
	 * the {@link Snake} object and initializes the first planet.
	 */
	public World() {
		this.snake = new Snake();
		placePlanet();
	}
	
	/**
	 * This method places a planet randomly on the game-board.
	 * It uses a 2d-array to check, whether a field is empty or not.
	 * An empty field is found, when fields[x][y] == false. 
	 */
	private void placePlanet() {
		int planetX, planetY;
		
		// At first all fields are empty.
		for (int x = 0; x < WORLD_WIDTH; x++) 
			for ( int y = 0; y < WORLD_HEIGHT; y++)
				fields[x][y] = false;
		
		// Mark fields with snake-parts as true.
		for (SnakePart part : snake.parts) 
			fields[part.x][part.y] = true;
		
		planetX = random.nextInt(WORLD_WIDTH);
		planetY = random.nextInt(WORLD_HEIGHT);
		
		// Find next free field randomly
		while (true) {
			if (!fields[planetX][planetY]) 
				break;
			planetX += 1;
			if (planetX >= WORLD_WIDTH) {
				planetX = 0;
				planetY += 1;
				if (planetY >= WORLD_HEIGHT)
					planetY = 0;
			}
		}
		
		// Create new planet.
		planet = new Planet(random.nextInt(3), planetX, planetY);
	}
	
	/**
	 * This method updates the state of the world. It checks
	 * if the game is over and moves the advances the snake.
	 * Also it checks, if a planet was eaten.
	 * @param deltaTime
	 * 	deltaTime = lastFrameTime - currentTime. Needed for frame independent
	 * 	movement.
	 */
	public void update(float deltaTime) {
		SnakePart head;
		
		if (gameOver) return;
		
		tickTime += deltaTime;
		
		// This loop performes the movement and game behaviour 
		// in between two frames. remember: the smaller the tick,
		// so higher the amount of iterations and more movement is
		// done. A higher speed is the result.
		while (tickTime > tick) {
			tickTime -= tick;
			snake.advance();
			
			if (snake.isDying()) {
				gameOver = true;
				return;
			}
			
			head = snake.parts.get(0);
			
			if (head.x == planet.x && head.y == planet.y) {
				snake.eat();
				if (Settings.getInstance().getControllMode() == Settings.CONTROLL_WEIRD)
					score += SCORE_INCREMENT * 2;
				else
					score += SCORE_INCREMENT;
				if (snake.parts.size() == WORLD_HEIGHT * WORLD_WIDTH) {
					gameOver = true;
					return;
				}
				else placePlanet();
				if (score % 50 == 0 && tick > 0) {
					tick -= TICK_DECREMENT;
				}
			}
		}
	}
	
}
