package name.tlink.android.games.spacesnake;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import name.tlink.android.game.framework.Screen;
import name.tlink.android.game.framework.impl.AndroidGame;

/**
 * This class extends {@link AndroidGame} which extends {@link Activity}.
 * So this is ourmain-activity.
 * 
 * @author Timon Link <a href="mailto:timon.link@gmail.com">timon.link@gmail.com</a>
 * @date 25.08.2014
 * @version 1.0
 */
public class SpaceSnakeGame extends AndroidGame {

	/**
	 * Start the game!
	 */
	@Override
	public Screen getStartScreen() {
		return new LoadingScreen(this);
	}

	/**
	 * Asks the user if he really wants to quit the
	 * game when the back key was pressed.
	 */
	@Override
	public void onBackPressed() {
		onPause();
		new AlertDialog.Builder(this)
				.setMessage(R.string.quit_game)
				.setPositiveButton(R.string.quit_game_yes,
						new OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								SpaceSnakeGame.this.finish();
								onResume();
							}
						})
				.setNegativeButton(R.string.quit_game_no,
						new OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								onResume();
							}
						}).show();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		if (isFinishing())
			Settings.getInstance().save(this.getFileIO());
	}
}
