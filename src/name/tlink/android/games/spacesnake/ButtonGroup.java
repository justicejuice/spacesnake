package name.tlink.android.games.spacesnake;

import java.util.ArrayList;
import java.util.List;

import name.tlink.android.game.framework.Graphics;
import name.tlink.android.game.framework.Input.TouchEvent;

public class ButtonGroup {
	
	public static final int ARRANGE_HORIZONTAL = 0;
	public static final int ARRANGE_VERTICAL = 1;
	
	List<SpaceButton> buttons;
	int x;
	int y;
	Graphics graphics;
	public ButtonGroup(Graphics graphics, int x, int y) {
		super();
		this.buttons = new ArrayList<SpaceButton>();
		this.graphics = graphics;
		this.x = x;
		this.y = y;
	}
	
	public void addButton(final SpaceButton button) {
		button.setOnClickAction(new MButtonListener() {
			@Override
			public void onClick() {
				btnClick(button);
			}
		});
		this.buttons.add(button);
	}
	
	public void draw(int arrangement) {
		int length = buttons.size();
		int btn_x, btn_y;
		SpaceButton btn = buttons.get(0);
		SpaceButton prev;
		btn.x = x;
		btn.y = y;
		btn.draw();
		
		for (int i = 1; i < length; i++) {
			btn = buttons.get(i);
			prev = buttons.get(i-1);
			if (arrangement == ARRANGE_HORIZONTAL) {
				btn_x = x + prev.x + prev.width;
				btn_y = y;
			}
			else {
				btn_x = x;
				btn_y  = prev.y + prev.height;
			}
			
			btn.x = btn_x;
			btn.y = btn_y;
			btn.draw();
			
		}
		
	}
	
	private void btnClick(SpaceButton button) {
		for (SpaceButton spaceButton : buttons) {
			spaceButton.setInactive();
		}
		button.setActive();
	}
	
	public boolean checkClick(TouchEvent event) {
		boolean ret = false;
		for (SpaceButton spaceButton : buttons) {
			if (spaceButton.click(event)) {
				ret = true;
				break;
			}
		}
		return ret;
	}
	
	public SpaceButton getSelectedButton() {
		SpaceButton res = null;
		for (SpaceButton spaceButton : buttons) {
			if (spaceButton.isActive) {
				res = spaceButton;
				break;
			}
		}
		return res;
	}
	
	public void setSelectedButton(int id) {
		for (SpaceButton spaceButton : buttons) {
			if (spaceButton.getId() == id)
				spaceButton.setActive();
			else
				spaceButton.setInactive();
		}
	}
	
}
