package name.tlink.android.games.spacesnake;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a Snake which grows by eating planets and moves
 * around.
 * 
 * @author Timon Link <a href="mailto:timon.link@gmail.com">timon.link@gmail.com</a>
 * @date 25.08.2014
 * @version 1.0
 *
 */
public class Snake {
	
	/**
	 * {@link Snake} moves up.
	 */
	public static final int DIR_UP = 0;
	
	/**
	 * {@link Snake} moves left.
	 */
	public static final int DIR_LEFT = 1;
	
	/**
	 * {@link Snake} moves down.
	 */
	public static final int DIR_DOWN = 2;
	
	/**
	 * {@link Snake} moves right.
	 */
	public static final int DIR_RIGHT = 3;
	
	/**
	 * The {@link Snake} consists of {@link SnakePart}s which are stored
	 * in this {@link List}.
	 */
	List<SnakePart> parts = new ArrayList<SnakePart>();
	
	/**
	 * The current direction of the {@link Snake}.
	 */
	public int direction;
	
	/**
	 * Creates a new instande of {@link Snake} and sets its
	 * start-position.
	 */
	public Snake() {
		direction = DIR_RIGHT;
		parts.add(new SnakePart(6, 5));
		parts.add(new SnakePart(5, 5));
		parts.add(new SnakePart(4, 5));
	}
	
	/**
	 * Turns the {@link Snake} right.
	 */
	public void turnRight() {
		direction -= 1;
		if (direction < DIR_UP)
			direction = DIR_RIGHT;
	}
	
	/**
	 * Turns the {@link Snake} left.
	 */
	public void turnLeft() {
		direction += 1;
		if (direction > DIR_RIGHT)
			direction = DIR_UP;
	}
	
	/**
	 * Adds a new {@link SnakePart} as tail to the {@link Snake}.
	 */
	public void eat() {
		SnakePart last = parts.get(parts.size() -1);
		parts.add(new SnakePart(last.x, last.y));
	}
	
	/**
	 * Moves the {@link Snake}.
	 */
	public void advance() {
		SnakePart head = parts.get(0);
		int len = parts.size() - 1;
		
		for (int i = len; i > 0; i--) {
			SnakePart before = parts.get(i-1);
			SnakePart part = parts.get(i);
			part.x = before.x;
			part.y = before.y;
		}
		
		if (direction == DIR_UP)
			head.y -= 1;
		if (direction == DIR_LEFT)
			head.x -= 1;
		if (direction == DIR_DOWN)
			head.y += 1;
		if (direction == DIR_RIGHT)
			head.x += 1;
		
		if (head.x < 0)
			head.x = World.WORLD_WIDTH - 1;
		if (head.y < 0)
			head.y = World.WORLD_HEIGHT - 1;
		if (head.x > World.WORLD_WIDTH - 1)
			head.x = 0;
		if (head.y > World.WORLD_HEIGHT - 1)
			head.y = 0;
	}
	
	/**
	 * Checks if the {@link Snake} is dying in this round.
	 * @return
	 * 	true - if the snake is dead, false otherwise.
	 */
	public boolean isDying() {
		int len = parts.size()-1;
		SnakePart head = parts.get(0);
		SnakePart current;
		for (int i=1; i<len; i++) {
			current = parts.get(i);
			if (head.x == current.x && head.y == current.y)
				return true;
		}
		return false;
	}
}
